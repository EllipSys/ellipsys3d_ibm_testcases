# Test case description

These test cases use the IBM and the k-omega SST model to simulate a tree at a Reynolds number based on tree-height of Re=4e5. 
All test cases use a standard wall function for rough walls with a roughness height of z0=1e-4, scales the outlet so that it matches the total mass flux across inlet and hole faces and use a probe point (PP) distance as specified in input.dat.

## Grid

The grid is cubic with a side length of 25H, where H is the tree height. The grid has 64 x 64 x 64 cells most of which are in the region around the tree. The grid is divided into 8 blocks with bsize=32. 
The flow direction is along the z-axis, the y-axis is vertically upwards and the x-axis is horizontal to the side.
 The boundary conditions are as follows: uniform velocity of w=1m/s at the inlet (z=0), Neumann at the outlet (z=25H), symmetry on the sides (x=0, x=25H) and on the bottom (y=0) and top (y=25H).

## IB grid

The IB grid surface is a coarse version of the tree surface used in the article "Immersed boundary method applied to flow past a tree skeleton". It is a simplified and downscaled model of real tree situated at Risø campus. The tree height is H=0.4m and the surface consists of 1782 triangles. 
   
## Case 1: log-rough-pp-org-outlet_scaling

The boundary conditions for velocity and pressure is set via a probe point (PP).

## Case 2: log-rough-pp-p1-outlet_scaling

The boundary conditions for velocity is set via a probe point (PP), whereas 1. order  extrapolation is used for the pressure.  

## Case 4: log-rough-pp-p2-outlet_scaling

The boundary conditions for velocity is set via a probe point (PP), whereas 2. order  extrapolation is used for the pressure.  

