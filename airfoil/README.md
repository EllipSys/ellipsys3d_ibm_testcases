# Test case description

These test cases use the IBM and the k-omega SST model to simulate an extruded NACA0012 airfoil at an angle of attack of 8 deg and Reynolds number of Re=6e6. All test cases use the default orientation and scaling of the IB surface. 

## Grid

The grid consists of an inner Cartesian mesh of size 1.0c x 0.12c inside which the airfoil is embedded, surrounded by an outer O-grid with it's outer boundary placed 30c away from the inner box. The grid is extruded 10c in the spanwise direction to obtain a 3D grid. Symmetry conditions are applied on the side boundaries. 
The number of cells in the inner box is Nx x Nz, the number of cells from the box to the outer boundary is denoted Nn and the number of spanwise cells is denoted by Ny. In the present test case the number of cells is Nx=128, Nz=64, Nn=32 and Ny=16, which results in a grid with 80 blocks of bsize=16.

## IB grid

The IB surface grid of the extruded NACA0012 airfoil consists of 512 triangles.

## Case 1: log-smooth-pp-org-linear-local_area

This test case uses a modified wall function for smooth walls where the inner part of the standard log-law is linearized. 
The boundary conditions for velocity and pressure is set via a probe point (PP).
The hole face mass flux correction scheme is based on the local area of the hole faces.
The test case uses the default grid dependent PP distance to the IB surface.

## Case 2: log-smooth-pp-org-outlet_scaling

This test case uses a standard logarithmic wall function for smooth walls. 
The boundary conditions for velocity and pressure is set via a probe point (PP).
The outlet is scaled so that it matches the total mass flux across inlet and hole faces. 
The PP distance to the IB surface for each grid level is specified in input.dat.

## Case 3: log-smooth-pp-p2-linear-local_flux   

This test case uses a modified wall function for smooth walls where the inner part of the standard log-law is linearized. 
The boundary conditions for velocity is set via a probe point (PP) whereas 2. order extrapolation is used for the pressure. 
The hole face mass flux correction scheme is based on the local flux through the hole faces.
The test case uses the default grid dependent PP distance to the IB surface.

## Case 4: log-smooth-pp-p2-local_flux

This test case uses a standard logarithmic wall function for smooth walls. 
The boundary conditions for velocity is set via a probe point (PP), whereas 2. order extrapolation is used for the pressure.
The hole face mass flux correction scheme is based on the local flux through the hole faces.
The test case uses the default grid dependent PP distance to the IB surface. 
