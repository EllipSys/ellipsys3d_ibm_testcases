      program makeinnerbox2d
      implicit none
      integer::i,ni,nr_dist_x,nr_dist_y,j,nj,bsize,xblock,yblock
      integer::l,xb,yb,attr,nbcout,nout
      real(kind=8)::xlen,ylen,hout,dh
      real(kind=8),allocatable,dimension(:)::dist_x,dist_y,x,y
      real(kind=8),allocatable,dimension(:,:)::xx,yy
      real(kind=8),dimension(10,3)::dist_inp_x,dist_inp_y
      open(unit=8,file='input.dat')
c---- make inner box
      read(8,*)
      read(8,*)xlen
      read(8,*)
      read(8,*)nr_dist_x,(dist_inp_x(i,1),dist_inp_x(i,2),
     &         dist_inp_x(i,3),i=1,nr_dist_x)
      read(8,*)
      read(8,*)ylen
      read(8,*)
      read(8,*)nr_dist_y,(dist_inp_y(i,1),dist_inp_y(i,2),
     &         dist_inp_y(i,3),i=1,nr_dist_y)
      read(8,*)
      read(8,*)bsize
      read(8,*)
      read(8,*)hout,dh
      read(8,*)
      read(8,*)nout

      close(8)
      ni=dist_inp_x(nr_dist_x,3); 
      nj=dist_inp_y(nr_dist_y,3); 
      allocate(dist_x(ni),x(ni),dist_y(nj),y(nj))
      call computedist(nr_dist_x,dist_inp_x(1:nr_dist_x,:),dist_x,ni)
      x=dist_x*xlen
      call computedist(nr_dist_y,dist_inp_y(1:nr_dist_y,:),dist_y,nj)
      y=dist_y*ylen
c---- generate sf.dat
      open(unit=8,file='sf.dat');
      write(8,*)'#',2*(ni+nj-2)+1
      do i=nj,1,-1;   write(8,*)x(ni),y(i ); enddo
      do i=ni-1,1,-1; write(8,*)x(i ),y(1 ); enddo
      do i=2,nj;      write(8,*)x(1 ),y(i ); enddo
      do i=2,ni;      write(8,*)x(i ),y(nj); enddo
      close(8)
c---- make x2d grid
      xblock=(ni-1)/bsize
      yblock=(nj-1)/bsize
      allocate(xx(bsize+1,xblock),yy(bsize+1,yblock));
c---- x-coords
      l=1;
      do xb=1,xblock;
         l=l-1;
         do i=1,bsize+1
            l=l+1; xx(i,xb)=x(l)
         enddo
      enddo
c---- y-coords
      l=1;
      do yb=1,yblock;
         l=l-1;
         do i=1,bsize+1
            l=l+1; yy(i,yb)=y(l)
         enddo
      enddo
      
      open(unit=8,file='box.x2d');
      attr=1;
      write(8,*)bsize,xblock*yblock
      do yb=1,yblock; do xb=1,xblock; do j=1,bsize+1; do i=1,bsize+1
         write(8,*)attr,xx(i,xb),yy(j,yb)
      enddo; enddo; enddo; enddo;
      close(8);
c---- Make gcf.dat file based on input
c      nbcout=(ni+nj-2)/2
      nbcout=nj-1
      open(unit=8,file='gcf.dat')
      write(8,*)'meshtype omesh-bcdyn'
      write(8,*)'block-size',bsize
      write(8,*)'meshpoints',32,nout
      write(8,*)'domain-height',hout,dh
      write(8,*)'stretching-function tanh'
c      write(8,*)'omesh-outlet',1,nbcout
      write(8,*)'volume-blend 0.005'
      write(8,*)'dissipation-factor 1.0'
      write(8,*)'explicit-smoothing 12 0.03'
      write(8,*)'wake-contraction 0.0'
      close(8)
       
      end
        
c-----------------------------------------------------------------------
      subroutine computedist(nr_dist_z,dist_inp_z,dist_z,nk)
      implicit none
      real(kind=8),dimension(2,3)::dinp
      real(kind=8),dimension(nr_dist_z,3)::dist_inp_z
      real(kind=8)::h1,h2,len
      real(kind=8),dimension(nk)::dist_z
      integer::i,nr_dist_z,nk

      if(nr_dist_z.gt.0)then
        call distfunc(nr_dist_z,dist_inp_z,nk,dist_z)
      else
        dinp(1,1)=0.d0;dinp(1,2)=1.d0/real(nk);dinp(1,3)=1.d0
        dinp(2,1)=1.d0;dinp(2,2)=1.d0/real(nk);dinp(2,3)=nk
        call distfunc(2,dinp,nk,dist_z)
      endif
      return
      end
c-----------------------------------------------------------------------
      subroutine distfunc(nn,dinp,ndist,fdist)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      implicit none
      integer::nn,ndist
      real(kind=8),dimension(ndist)::fdist,dy
      real(kind=8),dimension(nn,3)::dinp
      real(kind=8) s0,s1,d0,d1
      real(kind=8) len,delta1,delta2
      integer n0,n1,ns,nf
      integer i,j,ival

      ival=1

      fdist(1)=0.d0
      s0=dinp(1,1);d0=dinp(1,2);n0=int(dinp(1,3))
      do i=2,nn
        s1=dinp(i,1);d1=dinp(i,2);n1=int(dinp(i,3))
        len=s1-s0
        delta1=d0
        delta2=d1
        if(ival.eq.1)call tanhdist(delta1,delta2,len,n0,n1,dy(n0))
        if(ival.eq.2)call sinhdist(delta1,delta2,len,n0,n1,dy(n0))
        do j=n0+1,n1
        fdist(j)=fdist(n0)+dy(j)
        enddo
        s0=s1;d0=d1;n0=n1
      enddo
      return
   21 format('#',i6)
      end

      subroutine tanhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     TANDIST
*     program for computing the hyperbolic tangent distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8) delta1,delta2,len
      real(kind=8) delta,b,a
      real(kind=8) fdist(*)
      real(kind=8) transsinh,transtanh,ftmp
      external transsinh,transtanh
      integer i1,i2,ni,i


c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do 100 i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
  100     continue
        else
          delta=transtanh(b)
          do 101 i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
  101     continue
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do 200 i=1,ni+1
          fdist(i)=1.d0+tanh(.5d0*delta*(real(i-1)/real(ni)-1.d0))
     &            /tanh(.5d0*delta)
  200   continue
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do 300 i=1,ni+1
          fdist(i)=tanh(.5*delta*real(i-1)/real(ni))/tanh(.5*delta)
  300   continue
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do 1000 i=1,ni+1
        fdist(i)=fdist(i)*len
 1000 continue
      return
      end

      subroutine sinhdist(delta1,delta2,len,i1,i2,fdist)
************************************************************************
*     SINHDIST
*     program for computing the hyperbolic sine distribution
*     delta1 : grid spacing at i=i1 (first cell)
*     delta2 : grid spacing at i=i2 (last  cell)
*     len    : lenght of curve
*     i1     : indices of first vertex
*     i2     : indices of last vertex
*     fdist  : compute distribution function
*
*
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8) delta1,delta2,len
      real(kind=8) delta,b,a
      real(kind=8) fdist(*)
      real(kind=8) transsinh,transtanh,ftmp
      external transsinh,transtanh
      integer i1,i2,ni,i

c------- Normalize delta1 and delta2 with lenght
      delta1=delta1/len
      delta2=delta2/len
c------- Compute number of points
      ni=i2-i1

c------- Assure that B > 1
      if(delta1.le.0.d0.and.1/delta2.lt.ni)then
        delta1=1/(ni**2*delta2*1.02)
      elseif(delta2.le.0d0.and.1/delta1.lt.ni)then
        delta2=1/(ni**2*delta1*1.02)
      endif

c------- Spacing at both end specified
      if(delta1.gt.0.d0.and.delta2.gt.0.d0)then
        a=sqrt(delta2)/sqrt(delta1)
        b=1.d0/(ni*sqrt(delta1*delta2))
        if(b.ge.1.d0)then
          delta=transsinh(b)
          do 100 i=1,ni+1
            ftmp=.5*(1+tanh(delta*(real(i-1)/real(ni)-0.5))
     &          /tanh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
  100     continue
        else
          delta=transtanh(b)
          do 101 i=1,ni+1
            ftmp=.5*(1+sinh(delta*(real(i-1)/real(ni)-0.5))
     &          /sinh(.5*delta))
            fdist(i)=ftmp/(a+(1-a)*ftmp)
  101     continue
        endif
c------- Spacing only specified at xi=1
      else if(delta1.gt.0.d0)then
        b=1.d0/(ni*delta1)
        delta=transsinh(b)
        do 200 i=1,ni+1
          fdist(i)=sinh(delta*real(i-1)/real(ni))/sinh(delta)
  200   continue
c------- Spacing only specified at xi=ni
      else if(delta2.gt.0.d0)then
        b=1.d0/(ni*delta2)
        delta=transsinh(b)
        do 300 i=1,ni+1
          fdist(i)=1-sinh(delta*(1-real(i-1)/real(ni)))/sinh(delta)
  300   continue
c------- Error no spacing is given
      else
        print*,' Error from tandist, no cell hight is given '
      endif
c------- Compute len*fdist
      do 1000 i=1,ni+1
        fdist(i)=fdist(i)*len
 1000 continue
      return
      end

      real(kind=8) function transsinh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=sinh(delta)/delta
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8) delta,b
      real(kind=8) delta_old,f,df,rlim
      parameter(rlim=1.d-9)
      integer n

c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do 100 n=1,200
c--------- Compute f(delta)
c        f =b-sinh(delta)/delta
c        f =b*delta-sinh(delta)
         f =delta/sinh(delta)-1/b
c--------- Compute d/d(delta)(f(delta))
c        df=(sinh(delta)-delta*cosh(delta))/delta**2
c        df=b-cosh(delta)
         df=(sinh(delta)-delta*cosh(delta))/sinh(delta)**2
c--------- update delta
        delta=delta-f/(df+1.d-30)
c       write(*,'(4e16.4)')f,df,f/df,delta
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
  100 continue
  200 transsinh=delta
      return
      end

      real(kind=8) function transtanh(b)
************************************************************************
*     routine for solving the transendental equation
*
*     b=tanh(delta/2)/(delta/2)
*
*     using NewtonRaphson
************************************************************************
*     Author  : Niels N. Sorensen
*     Date    :
************************************************************************
      implicit none
      real(kind=8) delta,b
      real(kind=8) delta_old,f,df,rlim
      parameter(rlim=1.d-6)
      integer n

      if(b.gt.1.0d0)then
        print*,' Error form sinhdist: Can not be gridded '
c       stop
      endif
c------- Preforme Newton iteration to obtain delta
      delta=1.d0
      delta_old=delta
      do 100 n=1,200
c--------- Compute f(delta)
        f =delta/(tanh(.5*delta)+1.d-30)-2/B
c--------- Compute d/d(delta)(f(delta))
        df=(tanh(.5*delta)-delta*(1-tanh(.5*delta)**2))
     &    /(tanh(.5*delta)**2+1.d-30)
c--------- update delta
        delta=delta-f/(df+1.d-30)
c       write(*,'(i3,1x,3e16.4)')n,delta,f,df
c--------- check convergence criterium
c       if(abs((delta-delta_old)/delta_old).lt.rlim)goto 200
c--------- update old value
c       delta_old=delta
  100 continue
  200 transtanh=delta
      return
      end
