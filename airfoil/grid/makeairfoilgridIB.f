      program makeairfoilgridIB
      implicit none
      integer::i,j,ni,nj
      real*8,parameter::miny=0.0, maxy=9.999999
      real*8,allocatable,dimension(:)::x,z
      ni=2;
      open(unit=8,file='naca0012.dat');
      read(8,'(2x,i12)')nj
      allocate(x(nj),z(nj))
      do j=1,nj
         read(8,*)x(j),z(j)
      enddo
      close(8)
      open(unit=8,file='airfoil.dat') 
      write(8,*)(ni-1)*(nj-1)*2*3
      do j=1,nj-1
         write(8,1004)x(j),miny,z(j)
         write(8,1004)x(j),maxy,z(j)
         write(8,1004)x(j+1),maxy,z(j+1)
         write(8,1004)x(j),miny,z(j)
         write(8,1004)x(j+1),maxy,z(j+1)
         write(8,1004)x(j+1),miny,z(j+1)
      enddo;
      close(8)
 1004 format(6(1pe22.14))
      end
      
