# Test case description

These test cases use the IBM and the k-omega SST model to simulate a tower and nacelle at a Reynolds number based on tower+nacelle height of 1.24e8. Besides testing different IBM boundary conditions, these test cases also test the possibility to control the orientation and scaling of the immersed boundary (IB) as well as to model multiple IB's.

## Grid

The grid is cubic with a side length of 1000m. It has 64 x 64 x 64 cells and is divided into 8 blocks with bsize=32. The grid cells are concentrated in the region around the tower and nacelle. 
The flow direction is along the z-axis, the y-axis is vertically upwards and the x-axis is horizontal to the side. The boundary conditions are as follows: Uniform velocity of w=10m/s at the inlet (z=0), Neumann at the outlet (z=1000m), symmetry on the sides (x=0m, x=1000m) and on the bottom (y=0m) and top (y=1000m).

## IB grid

Three different IB surface grids are used:
1) A tower surface with a height of 115.6m and a bottom and top radius of 4.15m and 2.75m, respectively. The surface consists of 512 triangles.
2) A nacelle surface with dimensions W x H x L = 8.4m x 8.4m x 20m. The surface consists of 12 triangles
3) A combined tower and nacelle surface, which consists of 524 triangles 

The IB surface grids are defined in a coordinate system with x in the flow direction, y horizontal to the side and z vertically upwards. In all test cases the IB grids are therefore explicity turned to comply with the orientation of the background grid.   
   
## Case 1: log-smooth-nhc-p1-linear-local_area

This test case uses a modified wall function for smooth walls where the inner part of the standard log-law is linearized.
The boundary conditions for velocity is set based on it's near hole cell (NHC) value and the pressure boundary condition is set via 1. order extrapolation from the NHC.
The hole face mass flux correction scheme is based on the local area of the hole faces.
The tower and nacelle are modelled as two separate objects.

## Case 2: log-smooth-nhc-p1-local_area

This test case uses a standard wall function for smooth walls.
The boundary conditions for velocity is set based on it's near hole cell (NHC) value and the pressure boundary condition is set via 1. order extrapolation from the NHC.
The hole face mass flux correction scheme is based on the local area of the hole faces.
The tower and nacelle are modelled as one combined object.

## Case 3: log-smooth-nhc-p2-linear-local_area

This test case uses a modified wall function for smooth walls where the inner part of the standard log-law is linearized.
The boundary conditions for velocity is set based on it's near hole cell (NHC) value and the pressure boundary condition is set via 2. order extrapolation. Except for the linearized wall function, this approach is consistent with the standard treatment of boundary conditions on a body conforming grid.
The hole face mass flux correction scheme is based on the local area of the hole faces.
The tower and nacelle are modelled as one combined object.

## Case 4: log-smooth-nhc-p2-local_area

This test case uses a standard wall function for smooth walls.
The boundary conditions for velocity is set based on it's near hole cell (NHC) value and the pressure boundary condition is set via 2. order extrapolation. This approach is consistent with the standard treatment of boundary conditions on a body conforming grid.
The hole face mass flux correction scheme is based on the local area of the hole faces.
The tower and nacelle are modelled as one combined object.

## Case 5: log-smooth-pp-p1-linear-local_area

This test case uses a modified wall function for smooth walls where the inner part of the standard log-law is linearized.
The boundary conditions for velocity is set via a probe point (PP), whereas 1. order  extrapolation is used for the pressure.  
The hole face mass flux correction scheme is based on the local area of the hole faces.
The tower and nacelle are modelled as two separate objects.
The default grid dependent PP distance to the IB surfaces is used.

## Case 6: log-smooth-pp-p1-local_area

This test case uses a standard wall function for smooth walls.
The boundary conditions for velocity is set via a probe point (PP), whereas 1. order  extrapolation is used for the pressure.  
The hole face mass flux correction scheme is based on the local area of the hole faces.
The tower and nacelle are modelled as two separate objects.
The default grid dependent PP distance to the IB surfaces is used.
