      program maketowernacellegridIB
      implicit none
      integer::i,j,l
c---- definition of tower
      integer,parameter::ni=2,nj=128
      real*8,parameter::Ht=115.6, rbt=4.15, rtt=2.75
      real*8,allocatable,dimension(:,:)::xyzt
      real*8::theta,pi,dtheta,r(ni),x(ni,nj+1),y(ni,nj+1)
c---- definition of nacelle
      real*8,parameter::Ln=20.0,Wn=8.4, Hn=8.4 
      real*8,allocatable,dimension(:,:)::xyzn
      real*8,dimension(3)::posn
c-----------------------------------------------------------------------
c     tower grid
c-----------------------------------------------------------------------
      pi=4.0*atan(1.0)
      allocate(xyzt(2*ni*nj*3,3))
      dtheta=2.0*pi/dble(nj)
      r=(/rbt,rtt/)
      do i=1,ni; do j=1,nj+1
         theta=dble(j-1)*dtheta        
         x(i,j)=r(i)*cos(theta)
         y(i,j)=r(i)*sin(theta)
      enddo;enddo
      l=0;
c---- top      
      do j=1,nj         
         xyzt(l+1,:)=  (/0.d0,0.d0,Ht/);
         xyzt(l+2,:)=(/x(2,j),y(2,j),Ht/);
         xyzt(l+3,:)=(/x(2,j+1),y(2,j+1),Ht/)
         l=l+3
      enddo
c---- bottom
      do j=1,nj
         xyzt(l+1,:)=(/0.d0,0.d0,0.d0/)
         xyzt(l+2,:)=(/x(1,j),y(1,j),0.d0/)
         xyzt(l+3,:)=(/x(1,j+1),y(1,j+1),0.d0/)
         l=l+3
      enddo
c---- side
      do j=1,nj
         xyzt(l+1,:)=(/x(1,j),y(1,j),0.d0/)
         xyzt(l+2,:)=(/x(2,j),y(2,j),Ht/)
         xyzt(l+3,:)=(/x(2,j+1),y(2,j+1),Ht/)
         xyzt(l+4,:)=(/x(1,j),y(1,j),0.d0/)
         xyzt(l+5,:)=(/x(2,j+1),y(2,j+1),Ht/)
         xyzt(l+6,:)=(/x(1,j+1),y(1,j+1),0.d0/)
         l=l+6;
      enddo
c-----------------------------------------------------------------------
c     nacelle grid 
c-----------------------------------------------------------------------
      allocate(xyzn(6*2*3,3))
      l=0;
c---- nacelle face y1
      xyzn( 1,:)=(/-0.5*Ln,-0.5*Wn,0.d0/)
      xyzn( 2,:)=(/-0.5*Ln,-0.5*Wn,Hn/)
      xyzn( 3,:)=(/ 0.5*Ln,-0.5*Wn,Hn/)
      xyzn( 4,:)=(/-0.5*Ln,-0.5*Wn,0.d0/)
      xyzn( 5,:)=(/ 0.5*Ln,-0.5*Wn,Hn/)
      xyzn( 6,:)=(/ 0.5*Ln,-0.5*Wn,0.d0/)
c---- nacele face y2
      xyzn( 7,:)=(/-0.5*Ln, 0.5*Wn,0.d0/)
      xyzn( 8,:)=(/-0.5*Ln, 0.5*Wn,Hn/)
      xyzn( 9,:)=(/ 0.5*Ln, 0.5*Wn,Hn/)
      xyzn(10,:)=(/-0.5*Ln, 0.5*Wn,0.d0/)
      xyzn(11,:)=(/ 0.5*Ln, 0.5*Wn,Hn/)
      xyzn(12,:)=(/ 0.5*Ln, 0.5*Wn,0.d0/)
c---- nacelle face z1
      xyzn(13,:)=(/-0.5*Ln,-0.5*Wn,0.d0/)
      xyzn(14,:)=(/ 0.5*Ln, 0.5*Wn,0.d0/)
      xyzn(15,:)=(/-0.5*Ln, 0.5*Wn,0.d0/)
      xyzn(16,:)=(/-0.5*Ln,-0.5*Wn,0.d0/)
      xyzn(17,:)=(/ 0.5*Ln,-0.5*Wn,0.d0/)
      xyzn(18,:)=(/ 0.5*Ln, 0.5*Wn,0.d0/)
c---- nacelle face z2
      xyzn(19,:)=(/-0.5*Ln,-0.5*Wn,Hn/)
      xyzn(20,:)=(/ 0.5*Ln, 0.5*Wn,Hn/)
      xyzn(21,:)=(/-0.5*Ln, 0.5*Wn,Hn/)
      xyzn(22,:)=(/-0.5*Ln,-0.5*Wn,Hn/)
      xyzn(23,:)=(/ 0.5*Ln,-0.5*Wn,Hn/)
      xyzn(24,:)=(/ 0.5*Ln, 0.5*Wn,Hn/)
c---- nacelle face x1
      xyzn(25,:)=(/-0.5*Ln, 0.5*Wn,0.d0/)
      xyzn(26,:)=(/-0.5*Ln, 0.5*Wn,Hn/)
      xyzn(27,:)=(/-0.5*Ln,-0.5*Wn,Hn/)
      xyzn(28,:)=(/-0.5*Ln, 0.5*Wn,0.d0/)
      xyzn(29,:)=(/-0.5*Ln,-0.5*Wn,Hn/)
      xyzn(30,:)=(/-0.5*Ln,-0.5*Wn,0.d0/)
c---- nacelle face x2
      xyzn(31,:)=(/ 0.5*Ln, 0.5*Wn,0.d0/)
      xyzn(32,:)=(/ 0.5*Ln, 0.5*Wn,Hn/)
      xyzn(33,:)=(/ 0.5*Ln,-0.5*Wn,Hn/)
      xyzn(34,:)=(/ 0.5*Ln, 0.5*Wn,0.d0/)
      xyzn(35,:)=(/ 0.5*Ln,-0.5*Wn,Hn/)
      xyzn(36,:)=(/ 0.5*Ln,-0.5*Wn,0.d0/)
c-----------------------------------------------------------------------
c     write tower grid
c-----------------------------------------------------------------------
      open(unit=8,file='tower.dat')
      write(8,*)4*nj*3
      do j=1,4*nj*3
         write(8,1004)xyzt(j,:)
      enddo
      close(8)
c-----------------------------------------------------------------------
c     write nacelle grid
c-----------------------------------------------------------------------
      open(unit=8,file='nacelle.dat')
      write(8,*)6*2*3
      do j=1,6*2*3
         write(8,1004)xyzn(j,:)
      enddo
      close(8)
c-----------------------------------------------------------------------
c     write tower+nacelle grid
c-----------------------------------------------------------------------
      open(unit=8,file='towernacelle.dat')
      write(8,*)(4*nj+12)*3
      do j=1,4*nj*3
         write(8,1004)xyzt(j,:)
      enddo
      posn=0.d0; posn(3)=Ht;
      do j=1,6*2*3
         write(8,1004)xyzn(j,:)+posn
      enddo
      close(8)
 1004 format(6(1pe22.14))
      end
